const base_url = `http://localhost:3000`

export async function getMonth(year, month) {
    
    const response = await fetch(`${base_url}/${year}/${month}`, { 
        method: 'get' 
    })
    
    const data = await response.json()
    return data

}

export async function setDate(year, month, date, hours) {
    
    const body = (hours === '') ? null : JSON.stringify({ hours: hours })

    await fetch(`${base_url}/${year}/${month}/${date}`, { 
        method: 'post', 
        headers: { 'content-type': 'application/json' },
        body: body
    })

}