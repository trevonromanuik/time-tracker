const express = require('express')
const app = express()
const body_parser = require('body-parser')
const cors = require('cors');
const port = 3000

const fs = require('fs')
const path = require('path')
const data_path = path.resolve(__dirname, './data')

// parse application/x-www-form-urlencoded
app.use(body_parser.urlencoded({ extended: false }))

// parse application/json
app.use(body_parser.json())

app.use(cors());

app.get('/:year/:month', (req, res) => {

  const file_path = path.resolve(data_path, req.params.year, req.params.month)
  fs.readFile(file_path, (err, data) => {

    if (!err && data) {
      data = JSON.parse(data)
    }

    if (err && err.code === 'ENOENT') {
      err = null
      data = {}
    }

    if (err) return res.status(500).jsonp(JSON.stringify(err))
    res.jsonp(data)

  })

})

app.post('/:year/:month/:date', (req, res) => {

  const folder_path = path.resolve(data_path, req.params.year)
  const file_path = path.resolve(folder_path, req.params.month)
  fs.readFile(file_path, (err, data) => {

    if (!err && data) {
      data = JSON.parse(data)
    }

    if (err && err.code === 'ENOENT') {
      err = null
      data = {}
    }

    if (err) return res.status(500).jsonp(JSON.stringify(err))

    if (req.body.hasOwnProperty('hours')) {
      data[req.params.date] = req.body.hours
    } else {
      delete data[req.params.date]
    }

    fs.mkdir(folder_path, { recursive: true }, (err) => {

      if (err) return res.status(500).jsonp(JSON.stringify(err))

      fs.writeFile(file_path, JSON.stringify(data), (err) => {

        if (err) return res.status(500).jsonp(JSON.stringify(err))
        res.status(200).send()

      })

    })

  })

})

app.listen(port, () => {
  console.log(`time-tracker listening on port ${port}`)
})